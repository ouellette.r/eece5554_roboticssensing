#!/usr/bin/env python2

## Reads GPGGA data from GPS connected to serial port. Saves raw data and outputs parsed data.

import rospy
import serial
import utm

from gps_driver.msg import raw_lines
from gps_driver.msg import raw_data
from gps_driver.msg import parsed_data


def convert_to_degrees(hhmm_ss, orientation):
	if hhmm_ss[0]=='0':
		hh = hhmm_ss[1:3]
		mm = hhmm_ss[3:]
	else:
		hh = hhmm_ss[0:2]
		mm = hhmm_ss[2:]
	
	if orientation == 'S' or orientation == 'W':
		degrees = -1*(float(hh)+float(mm)/60)
	else:
		degrees = float(hh)+float(mm)/60

	return degrees
	

if __name__ == '__main__':
	
	rospy.init_node('gps_output', anonymous=True)
	

	port_name = input("Enter port name as a string(dont include /dev/): Example: ttyUSB0\n")
	port_address = '/dev/'+port_name

	serial_port = rospy.get_param('~port',port_address)
	serial_baud = rospy.get_param('~baudrate',4800)
	port = serial.Serial(serial_port, serial_baud, timeout=3.)
	
	pub_parsed_data = rospy.Publisher('parsed_data_topic', parsed_data, queue_size=10)
	parsed_msg = parsed_data()

	pub_raw_data = rospy.Publisher('raw_data_topic', raw_data, queue_size=10)
	raw_msg = raw_data()

	pub_raw_lines = rospy.Publisher('raw_lines_topic', raw_lines, queue_size=10)
	raw_line = raw_lines()

	try:
		while not rospy.is_shutdown():
			line = port.readline()

			raw_line.line = line
			pub_raw_lines.publish(raw_line)
			
			if line == '':
				rospy.loginfo("NO DATA")

			elif line.find("$GPGGA")!=-1:
				
				rospy.loginfo(line)

				parts = line.split(",")
				raw_msg.type = parts[0]
				raw_msg.utc_position = parts[1]
				raw_msg.lat = parts[2]
				raw_msg.n_s = parts[3]
				raw_msg.long = parts[4]
				raw_msg.e_w = parts[5]
				raw_msg.quality = parts[6]
				raw_msg.sats = parts[7]
				raw_msg.hdop = parts[8]
				raw_msg.alt = parts[9]
				raw_msg.units = parts[10]
				raw_msg.geo_sep = parts[11]

				pub_raw_data.publish(raw_msg)


				parsed_msg.header.stamp = rospy.Time.now()

				#convert latitude to degrees
				try:
					latitude = convert_to_degrees(parts[2],parts[3])
					parsed_msg.Latitude = str(latitude)

					#convert longitude to degrees
					longitude = convert_to_degrees(parts[4],parts[5])
					parsed_msg.Longitude = str(longitude)
					
					#altitude
					altitude = parts[9]
					parsed_msg.Altitude = str(altitude)

					#lat/long to utm
					utm_parts = utm.from_latlon(latitude,longitude)

					#utm easting
					parsed_msg.UTM_Easting = str(utm_parts[0])

					#utm northing
					parsed_msg.UTM_Northing = str(utm_parts[1])

					#utm zone
					parsed_msg.Zone = str(utm_parts[2])

					#utm letter
					parsed_msg.Letter = str(utm_parts[3])
					
					pub_parsed_data.publish(parsed_msg)
				except:
					rospy.loginfo("GPS unable to establish connection")
			else:
				rospy.loginfo(line)

	except rospy.ROSInterruptException:
		port.close()

	except serial.serialutil.SerialException:
		rospy.loginfo("Shutting down node...")
