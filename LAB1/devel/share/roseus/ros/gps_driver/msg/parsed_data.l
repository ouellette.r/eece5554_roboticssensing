;; Auto-generated. Do not edit!


(when (boundp 'gps_driver::parsed_data)
  (if (not (find-package "GPS_DRIVER"))
    (make-package "GPS_DRIVER"))
  (shadow 'parsed_data (find-package "GPS_DRIVER")))
(unless (find-package "GPS_DRIVER::PARSED_DATA")
  (make-package "GPS_DRIVER::PARSED_DATA"))

(in-package "ROS")
;;//! \htmlinclude parsed_data.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass gps_driver::parsed_data
  :super ros::object
  :slots (_header _Latitude _Longitude _Altitude _UTM_Easting _UTM_Northing _Zone _Letter ))

(defmethod gps_driver::parsed_data
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:Latitude __Latitude) "")
    ((:Longitude __Longitude) "")
    ((:Altitude __Altitude) "")
    ((:UTM_Easting __UTM_Easting) "")
    ((:UTM_Northing __UTM_Northing) "")
    ((:Zone __Zone) "")
    ((:Letter __Letter) "")
    )
   (send-super :init)
   (setq _header __header)
   (setq _Latitude (string __Latitude))
   (setq _Longitude (string __Longitude))
   (setq _Altitude (string __Altitude))
   (setq _UTM_Easting (string __UTM_Easting))
   (setq _UTM_Northing (string __UTM_Northing))
   (setq _Zone (string __Zone))
   (setq _Letter (string __Letter))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:Latitude
   (&optional __Latitude)
   (if __Latitude (setq _Latitude __Latitude)) _Latitude)
  (:Longitude
   (&optional __Longitude)
   (if __Longitude (setq _Longitude __Longitude)) _Longitude)
  (:Altitude
   (&optional __Altitude)
   (if __Altitude (setq _Altitude __Altitude)) _Altitude)
  (:UTM_Easting
   (&optional __UTM_Easting)
   (if __UTM_Easting (setq _UTM_Easting __UTM_Easting)) _UTM_Easting)
  (:UTM_Northing
   (&optional __UTM_Northing)
   (if __UTM_Northing (setq _UTM_Northing __UTM_Northing)) _UTM_Northing)
  (:Zone
   (&optional __Zone)
   (if __Zone (setq _Zone __Zone)) _Zone)
  (:Letter
   (&optional __Letter)
   (if __Letter (setq _Letter __Letter)) _Letter)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; string _Latitude
    4 (length _Latitude)
    ;; string _Longitude
    4 (length _Longitude)
    ;; string _Altitude
    4 (length _Altitude)
    ;; string _UTM_Easting
    4 (length _UTM_Easting)
    ;; string _UTM_Northing
    4 (length _UTM_Northing)
    ;; string _Zone
    4 (length _Zone)
    ;; string _Letter
    4 (length _Letter)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; string _Latitude
       (write-long (length _Latitude) s) (princ _Latitude s)
     ;; string _Longitude
       (write-long (length _Longitude) s) (princ _Longitude s)
     ;; string _Altitude
       (write-long (length _Altitude) s) (princ _Altitude s)
     ;; string _UTM_Easting
       (write-long (length _UTM_Easting) s) (princ _UTM_Easting s)
     ;; string _UTM_Northing
       (write-long (length _UTM_Northing) s) (princ _UTM_Northing s)
     ;; string _Zone
       (write-long (length _Zone) s) (princ _Zone s)
     ;; string _Letter
       (write-long (length _Letter) s) (princ _Letter s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; string _Latitude
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _Latitude (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _Longitude
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _Longitude (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _Altitude
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _Altitude (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _UTM_Easting
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _UTM_Easting (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _UTM_Northing
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _UTM_Northing (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _Zone
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _Zone (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _Letter
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _Letter (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get gps_driver::parsed_data :md5sum-) "0eab0c6a5e94593b788b36ff3576f1ef")
(setf (get gps_driver::parsed_data :datatype-) "gps_driver/parsed_data")
(setf (get gps_driver::parsed_data :definition-)
      "Header header
string Latitude
string Longitude
string Altitude
string UTM_Easting
string UTM_Northing
string Zone
string Letter


================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :gps_driver/parsed_data "0eab0c6a5e94593b788b36ff3576f1ef")


