;; Auto-generated. Do not edit!


(when (boundp 'gps_driver::raw_lines)
  (if (not (find-package "GPS_DRIVER"))
    (make-package "GPS_DRIVER"))
  (shadow 'raw_lines (find-package "GPS_DRIVER")))
(unless (find-package "GPS_DRIVER::RAW_LINES")
  (make-package "GPS_DRIVER::RAW_LINES"))

(in-package "ROS")
;;//! \htmlinclude raw_lines.msg.html


(defclass gps_driver::raw_lines
  :super ros::object
  :slots (_line ))

(defmethod gps_driver::raw_lines
  (:init
   (&key
    ((:line __line) "")
    )
   (send-super :init)
   (setq _line (string __line))
   self)
  (:line
   (&optional __line)
   (if __line (setq _line __line)) _line)
  (:serialization-length
   ()
   (+
    ;; string _line
    4 (length _line)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _line
       (write-long (length _line) s) (princ _line s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _line
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _line (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get gps_driver::raw_lines :md5sum-) "5c74f2920fe696c465ee011cedd4ca6e")
(setf (get gps_driver::raw_lines :datatype-) "gps_driver/raw_lines")
(setf (get gps_driver::raw_lines :definition-)
      "string line

")



(provide :gps_driver/raw_lines "5c74f2920fe696c465ee011cedd4ca6e")


