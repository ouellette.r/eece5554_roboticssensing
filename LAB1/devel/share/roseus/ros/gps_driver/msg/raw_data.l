;; Auto-generated. Do not edit!


(when (boundp 'gps_driver::raw_data)
  (if (not (find-package "GPS_DRIVER"))
    (make-package "GPS_DRIVER"))
  (shadow 'raw_data (find-package "GPS_DRIVER")))
(unless (find-package "GPS_DRIVER::RAW_DATA")
  (make-package "GPS_DRIVER::RAW_DATA"))

(in-package "ROS")
;;//! \htmlinclude raw_data.msg.html


(defclass gps_driver::raw_data
  :super ros::object
  :slots (_type _utc_position _lat _n_s _long _e_w _quality _sats _hdop _alt _units _geo_sep ))

(defmethod gps_driver::raw_data
  (:init
   (&key
    ((:type __type) "")
    ((:utc_position __utc_position) "")
    ((:lat __lat) "")
    ((:n_s __n_s) "")
    ((:long __long) "")
    ((:e_w __e_w) "")
    ((:quality __quality) "")
    ((:sats __sats) "")
    ((:hdop __hdop) "")
    ((:alt __alt) "")
    ((:units __units) "")
    ((:geo_sep __geo_sep) "")
    )
   (send-super :init)
   (setq _type (string __type))
   (setq _utc_position (string __utc_position))
   (setq _lat (string __lat))
   (setq _n_s (string __n_s))
   (setq _long (string __long))
   (setq _e_w (string __e_w))
   (setq _quality (string __quality))
   (setq _sats (string __sats))
   (setq _hdop (string __hdop))
   (setq _alt (string __alt))
   (setq _units (string __units))
   (setq _geo_sep (string __geo_sep))
   self)
  (:type
   (&optional __type)
   (if __type (setq _type __type)) _type)
  (:utc_position
   (&optional __utc_position)
   (if __utc_position (setq _utc_position __utc_position)) _utc_position)
  (:lat
   (&optional __lat)
   (if __lat (setq _lat __lat)) _lat)
  (:n_s
   (&optional __n_s)
   (if __n_s (setq _n_s __n_s)) _n_s)
  (:long
   (&optional __long)
   (if __long (setq _long __long)) _long)
  (:e_w
   (&optional __e_w)
   (if __e_w (setq _e_w __e_w)) _e_w)
  (:quality
   (&optional __quality)
   (if __quality (setq _quality __quality)) _quality)
  (:sats
   (&optional __sats)
   (if __sats (setq _sats __sats)) _sats)
  (:hdop
   (&optional __hdop)
   (if __hdop (setq _hdop __hdop)) _hdop)
  (:alt
   (&optional __alt)
   (if __alt (setq _alt __alt)) _alt)
  (:units
   (&optional __units)
   (if __units (setq _units __units)) _units)
  (:geo_sep
   (&optional __geo_sep)
   (if __geo_sep (setq _geo_sep __geo_sep)) _geo_sep)
  (:serialization-length
   ()
   (+
    ;; string _type
    4 (length _type)
    ;; string _utc_position
    4 (length _utc_position)
    ;; string _lat
    4 (length _lat)
    ;; string _n_s
    4 (length _n_s)
    ;; string _long
    4 (length _long)
    ;; string _e_w
    4 (length _e_w)
    ;; string _quality
    4 (length _quality)
    ;; string _sats
    4 (length _sats)
    ;; string _hdop
    4 (length _hdop)
    ;; string _alt
    4 (length _alt)
    ;; string _units
    4 (length _units)
    ;; string _geo_sep
    4 (length _geo_sep)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _type
       (write-long (length _type) s) (princ _type s)
     ;; string _utc_position
       (write-long (length _utc_position) s) (princ _utc_position s)
     ;; string _lat
       (write-long (length _lat) s) (princ _lat s)
     ;; string _n_s
       (write-long (length _n_s) s) (princ _n_s s)
     ;; string _long
       (write-long (length _long) s) (princ _long s)
     ;; string _e_w
       (write-long (length _e_w) s) (princ _e_w s)
     ;; string _quality
       (write-long (length _quality) s) (princ _quality s)
     ;; string _sats
       (write-long (length _sats) s) (princ _sats s)
     ;; string _hdop
       (write-long (length _hdop) s) (princ _hdop s)
     ;; string _alt
       (write-long (length _alt) s) (princ _alt s)
     ;; string _units
       (write-long (length _units) s) (princ _units s)
     ;; string _geo_sep
       (write-long (length _geo_sep) s) (princ _geo_sep s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _type
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _type (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _utc_position
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _utc_position (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _lat
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _lat (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _n_s
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _n_s (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _long
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _long (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _e_w
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _e_w (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _quality
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _quality (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _sats
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _sats (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _hdop
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _hdop (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _alt
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _alt (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _units
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _units (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _geo_sep
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _geo_sep (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get gps_driver::raw_data :md5sum-) "a8fc4d15e90cc4fb2818dc101c57a5bb")
(setf (get gps_driver::raw_data :datatype-) "gps_driver/raw_data")
(setf (get gps_driver::raw_data :definition-)
      "string type
string utc_position 
string lat
string n_s
string long
string e_w
string quality
string sats
string hdop
string alt
string units
string geo_sep

")



(provide :gps_driver/raw_data "a8fc4d15e90cc4fb2818dc101c57a5bb")


