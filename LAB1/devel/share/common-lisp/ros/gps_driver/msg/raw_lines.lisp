; Auto-generated. Do not edit!


(cl:in-package gps_driver-msg)


;//! \htmlinclude raw_lines.msg.html

(cl:defclass <raw_lines> (roslisp-msg-protocol:ros-message)
  ((line
    :reader line
    :initarg :line
    :type cl:string
    :initform ""))
)

(cl:defclass raw_lines (<raw_lines>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <raw_lines>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'raw_lines)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gps_driver-msg:<raw_lines> is deprecated: use gps_driver-msg:raw_lines instead.")))

(cl:ensure-generic-function 'line-val :lambda-list '(m))
(cl:defmethod line-val ((m <raw_lines>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_driver-msg:line-val is deprecated.  Use gps_driver-msg:line instead.")
  (line m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <raw_lines>) ostream)
  "Serializes a message object of type '<raw_lines>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'line))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'line))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <raw_lines>) istream)
  "Deserializes a message object of type '<raw_lines>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'line) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'line) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<raw_lines>)))
  "Returns string type for a message object of type '<raw_lines>"
  "gps_driver/raw_lines")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'raw_lines)))
  "Returns string type for a message object of type 'raw_lines"
  "gps_driver/raw_lines")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<raw_lines>)))
  "Returns md5sum for a message object of type '<raw_lines>"
  "5c74f2920fe696c465ee011cedd4ca6e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'raw_lines)))
  "Returns md5sum for a message object of type 'raw_lines"
  "5c74f2920fe696c465ee011cedd4ca6e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<raw_lines>)))
  "Returns full string definition for message of type '<raw_lines>"
  (cl:format cl:nil "string line~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'raw_lines)))
  "Returns full string definition for message of type 'raw_lines"
  (cl:format cl:nil "string line~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <raw_lines>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'line))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <raw_lines>))
  "Converts a ROS message object to a list"
  (cl:list 'raw_lines
    (cl:cons ':line (line msg))
))
