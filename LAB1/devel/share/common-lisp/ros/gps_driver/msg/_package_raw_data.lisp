(cl:in-package gps_driver-msg)
(cl:export '(TYPE-VAL
          TYPE
          UTC_POSITION-VAL
          UTC_POSITION
          LAT-VAL
          LAT
          N_S-VAL
          N_S
          LONG-VAL
          LONG
          E_W-VAL
          E_W
          QUALITY-VAL
          QUALITY
          SATS-VAL
          SATS
          HDOP-VAL
          HDOP
          ALT-VAL
          ALT
          UNITS-VAL
          UNITS
          GEO_SEP-VAL
          GEO_SEP
))