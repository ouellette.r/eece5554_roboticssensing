// Auto-generated. Do not edit!

// (in-package gps_driver.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class raw_data {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.type = null;
      this.utc_position = null;
      this.lat = null;
      this.n_s = null;
      this.long = null;
      this.e_w = null;
      this.quality = null;
      this.sats = null;
      this.hdop = null;
      this.alt = null;
      this.units = null;
      this.geo_sep = null;
    }
    else {
      if (initObj.hasOwnProperty('type')) {
        this.type = initObj.type
      }
      else {
        this.type = '';
      }
      if (initObj.hasOwnProperty('utc_position')) {
        this.utc_position = initObj.utc_position
      }
      else {
        this.utc_position = '';
      }
      if (initObj.hasOwnProperty('lat')) {
        this.lat = initObj.lat
      }
      else {
        this.lat = '';
      }
      if (initObj.hasOwnProperty('n_s')) {
        this.n_s = initObj.n_s
      }
      else {
        this.n_s = '';
      }
      if (initObj.hasOwnProperty('long')) {
        this.long = initObj.long
      }
      else {
        this.long = '';
      }
      if (initObj.hasOwnProperty('e_w')) {
        this.e_w = initObj.e_w
      }
      else {
        this.e_w = '';
      }
      if (initObj.hasOwnProperty('quality')) {
        this.quality = initObj.quality
      }
      else {
        this.quality = '';
      }
      if (initObj.hasOwnProperty('sats')) {
        this.sats = initObj.sats
      }
      else {
        this.sats = '';
      }
      if (initObj.hasOwnProperty('hdop')) {
        this.hdop = initObj.hdop
      }
      else {
        this.hdop = '';
      }
      if (initObj.hasOwnProperty('alt')) {
        this.alt = initObj.alt
      }
      else {
        this.alt = '';
      }
      if (initObj.hasOwnProperty('units')) {
        this.units = initObj.units
      }
      else {
        this.units = '';
      }
      if (initObj.hasOwnProperty('geo_sep')) {
        this.geo_sep = initObj.geo_sep
      }
      else {
        this.geo_sep = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type raw_data
    // Serialize message field [type]
    bufferOffset = _serializer.string(obj.type, buffer, bufferOffset);
    // Serialize message field [utc_position]
    bufferOffset = _serializer.string(obj.utc_position, buffer, bufferOffset);
    // Serialize message field [lat]
    bufferOffset = _serializer.string(obj.lat, buffer, bufferOffset);
    // Serialize message field [n_s]
    bufferOffset = _serializer.string(obj.n_s, buffer, bufferOffset);
    // Serialize message field [long]
    bufferOffset = _serializer.string(obj.long, buffer, bufferOffset);
    // Serialize message field [e_w]
    bufferOffset = _serializer.string(obj.e_w, buffer, bufferOffset);
    // Serialize message field [quality]
    bufferOffset = _serializer.string(obj.quality, buffer, bufferOffset);
    // Serialize message field [sats]
    bufferOffset = _serializer.string(obj.sats, buffer, bufferOffset);
    // Serialize message field [hdop]
    bufferOffset = _serializer.string(obj.hdop, buffer, bufferOffset);
    // Serialize message field [alt]
    bufferOffset = _serializer.string(obj.alt, buffer, bufferOffset);
    // Serialize message field [units]
    bufferOffset = _serializer.string(obj.units, buffer, bufferOffset);
    // Serialize message field [geo_sep]
    bufferOffset = _serializer.string(obj.geo_sep, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type raw_data
    let len;
    let data = new raw_data(null);
    // Deserialize message field [type]
    data.type = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [utc_position]
    data.utc_position = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [lat]
    data.lat = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [n_s]
    data.n_s = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [long]
    data.long = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [e_w]
    data.e_w = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [quality]
    data.quality = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [sats]
    data.sats = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [hdop]
    data.hdop = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [alt]
    data.alt = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [units]
    data.units = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [geo_sep]
    data.geo_sep = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.type.length;
    length += object.utc_position.length;
    length += object.lat.length;
    length += object.n_s.length;
    length += object.long.length;
    length += object.e_w.length;
    length += object.quality.length;
    length += object.sats.length;
    length += object.hdop.length;
    length += object.alt.length;
    length += object.units.length;
    length += object.geo_sep.length;
    return length + 48;
  }

  static datatype() {
    // Returns string type for a message object
    return 'gps_driver/raw_data';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'a8fc4d15e90cc4fb2818dc101c57a5bb';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string type
    string utc_position 
    string lat
    string n_s
    string long
    string e_w
    string quality
    string sats
    string hdop
    string alt
    string units
    string geo_sep
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new raw_data(null);
    if (msg.type !== undefined) {
      resolved.type = msg.type;
    }
    else {
      resolved.type = ''
    }

    if (msg.utc_position !== undefined) {
      resolved.utc_position = msg.utc_position;
    }
    else {
      resolved.utc_position = ''
    }

    if (msg.lat !== undefined) {
      resolved.lat = msg.lat;
    }
    else {
      resolved.lat = ''
    }

    if (msg.n_s !== undefined) {
      resolved.n_s = msg.n_s;
    }
    else {
      resolved.n_s = ''
    }

    if (msg.long !== undefined) {
      resolved.long = msg.long;
    }
    else {
      resolved.long = ''
    }

    if (msg.e_w !== undefined) {
      resolved.e_w = msg.e_w;
    }
    else {
      resolved.e_w = ''
    }

    if (msg.quality !== undefined) {
      resolved.quality = msg.quality;
    }
    else {
      resolved.quality = ''
    }

    if (msg.sats !== undefined) {
      resolved.sats = msg.sats;
    }
    else {
      resolved.sats = ''
    }

    if (msg.hdop !== undefined) {
      resolved.hdop = msg.hdop;
    }
    else {
      resolved.hdop = ''
    }

    if (msg.alt !== undefined) {
      resolved.alt = msg.alt;
    }
    else {
      resolved.alt = ''
    }

    if (msg.units !== undefined) {
      resolved.units = msg.units;
    }
    else {
      resolved.units = ''
    }

    if (msg.geo_sep !== undefined) {
      resolved.geo_sep = msg.geo_sep;
    }
    else {
      resolved.geo_sep = ''
    }

    return resolved;
    }
};

module.exports = raw_data;
