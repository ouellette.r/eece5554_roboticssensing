// Auto-generated. Do not edit!

// (in-package gps_driver.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class parsed_data {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.Latitude = null;
      this.Longitude = null;
      this.Altitude = null;
      this.UTM_Easting = null;
      this.UTM_Northing = null;
      this.Zone = null;
      this.Letter = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('Latitude')) {
        this.Latitude = initObj.Latitude
      }
      else {
        this.Latitude = '';
      }
      if (initObj.hasOwnProperty('Longitude')) {
        this.Longitude = initObj.Longitude
      }
      else {
        this.Longitude = '';
      }
      if (initObj.hasOwnProperty('Altitude')) {
        this.Altitude = initObj.Altitude
      }
      else {
        this.Altitude = '';
      }
      if (initObj.hasOwnProperty('UTM_Easting')) {
        this.UTM_Easting = initObj.UTM_Easting
      }
      else {
        this.UTM_Easting = '';
      }
      if (initObj.hasOwnProperty('UTM_Northing')) {
        this.UTM_Northing = initObj.UTM_Northing
      }
      else {
        this.UTM_Northing = '';
      }
      if (initObj.hasOwnProperty('Zone')) {
        this.Zone = initObj.Zone
      }
      else {
        this.Zone = '';
      }
      if (initObj.hasOwnProperty('Letter')) {
        this.Letter = initObj.Letter
      }
      else {
        this.Letter = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type parsed_data
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [Latitude]
    bufferOffset = _serializer.string(obj.Latitude, buffer, bufferOffset);
    // Serialize message field [Longitude]
    bufferOffset = _serializer.string(obj.Longitude, buffer, bufferOffset);
    // Serialize message field [Altitude]
    bufferOffset = _serializer.string(obj.Altitude, buffer, bufferOffset);
    // Serialize message field [UTM_Easting]
    bufferOffset = _serializer.string(obj.UTM_Easting, buffer, bufferOffset);
    // Serialize message field [UTM_Northing]
    bufferOffset = _serializer.string(obj.UTM_Northing, buffer, bufferOffset);
    // Serialize message field [Zone]
    bufferOffset = _serializer.string(obj.Zone, buffer, bufferOffset);
    // Serialize message field [Letter]
    bufferOffset = _serializer.string(obj.Letter, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type parsed_data
    let len;
    let data = new parsed_data(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [Latitude]
    data.Latitude = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [Longitude]
    data.Longitude = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [Altitude]
    data.Altitude = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [UTM_Easting]
    data.UTM_Easting = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [UTM_Northing]
    data.UTM_Northing = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [Zone]
    data.Zone = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [Letter]
    data.Letter = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += object.Latitude.length;
    length += object.Longitude.length;
    length += object.Altitude.length;
    length += object.UTM_Easting.length;
    length += object.UTM_Northing.length;
    length += object.Zone.length;
    length += object.Letter.length;
    return length + 28;
  }

  static datatype() {
    // Returns string type for a message object
    return 'gps_driver/parsed_data';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '0eab0c6a5e94593b788b36ff3576f1ef';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    string Latitude
    string Longitude
    string Altitude
    string UTM_Easting
    string UTM_Northing
    string Zone
    string Letter
    
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new parsed_data(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.Latitude !== undefined) {
      resolved.Latitude = msg.Latitude;
    }
    else {
      resolved.Latitude = ''
    }

    if (msg.Longitude !== undefined) {
      resolved.Longitude = msg.Longitude;
    }
    else {
      resolved.Longitude = ''
    }

    if (msg.Altitude !== undefined) {
      resolved.Altitude = msg.Altitude;
    }
    else {
      resolved.Altitude = ''
    }

    if (msg.UTM_Easting !== undefined) {
      resolved.UTM_Easting = msg.UTM_Easting;
    }
    else {
      resolved.UTM_Easting = ''
    }

    if (msg.UTM_Northing !== undefined) {
      resolved.UTM_Northing = msg.UTM_Northing;
    }
    else {
      resolved.UTM_Northing = ''
    }

    if (msg.Zone !== undefined) {
      resolved.Zone = msg.Zone;
    }
    else {
      resolved.Zone = ''
    }

    if (msg.Letter !== undefined) {
      resolved.Letter = msg.Letter;
    }
    else {
      resolved.Letter = ''
    }

    return resolved;
    }
};

module.exports = parsed_data;
