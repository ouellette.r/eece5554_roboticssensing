// Auto-generated. Do not edit!

// (in-package gps_driver.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class raw_lines {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.line = null;
    }
    else {
      if (initObj.hasOwnProperty('line')) {
        this.line = initObj.line
      }
      else {
        this.line = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type raw_lines
    // Serialize message field [line]
    bufferOffset = _serializer.string(obj.line, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type raw_lines
    let len;
    let data = new raw_lines(null);
    // Deserialize message field [line]
    data.line = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.line.length;
    return length + 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'gps_driver/raw_lines';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '5c74f2920fe696c465ee011cedd4ca6e';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string line
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new raw_lines(null);
    if (msg.line !== undefined) {
      resolved.line = msg.line;
    }
    else {
      resolved.line = ''
    }

    return resolved;
    }
};

module.exports = raw_lines;
