// Generated by gencpp from file gps_driver/raw_data.msg
// DO NOT EDIT!


#ifndef GPS_DRIVER_MESSAGE_RAW_DATA_H
#define GPS_DRIVER_MESSAGE_RAW_DATA_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace gps_driver
{
template <class ContainerAllocator>
struct raw_data_
{
  typedef raw_data_<ContainerAllocator> Type;

  raw_data_()
    : type()
    , utc_position()
    , lat()
    , n_s()
    , long()
    , e_w()
    , quality()
    , sats()
    , hdop()
    , alt()
    , units()
    , geo_sep()  {
    }
  raw_data_(const ContainerAllocator& _alloc)
    : type(_alloc)
    , utc_position(_alloc)
    , lat(_alloc)
    , n_s(_alloc)
    , long(_alloc)
    , e_w(_alloc)
    , quality(_alloc)
    , sats(_alloc)
    , hdop(_alloc)
    , alt(_alloc)
    , units(_alloc)
    , geo_sep(_alloc)  {
  (void)_alloc;
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _type_type;
  _type_type type;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _utc_position_type;
  _utc_position_type utc_position;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _lat_type;
  _lat_type lat;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _n_s_type;
  _n_s_type n_s;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _long_type;
  _long_type long;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _e_w_type;
  _e_w_type e_w;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _quality_type;
  _quality_type quality;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _sats_type;
  _sats_type sats;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _hdop_type;
  _hdop_type hdop;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _alt_type;
  _alt_type alt;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _units_type;
  _units_type units;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _geo_sep_type;
  _geo_sep_type geo_sep;





  typedef boost::shared_ptr< ::gps_driver::raw_data_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::gps_driver::raw_data_<ContainerAllocator> const> ConstPtr;

}; // struct raw_data_

typedef ::gps_driver::raw_data_<std::allocator<void> > raw_data;

typedef boost::shared_ptr< ::gps_driver::raw_data > raw_dataPtr;
typedef boost::shared_ptr< ::gps_driver::raw_data const> raw_dataConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::gps_driver::raw_data_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::gps_driver::raw_data_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::gps_driver::raw_data_<ContainerAllocator1> & lhs, const ::gps_driver::raw_data_<ContainerAllocator2> & rhs)
{
  return lhs.type == rhs.type &&
    lhs.utc_position == rhs.utc_position &&
    lhs.lat == rhs.lat &&
    lhs.n_s == rhs.n_s &&
    lhs.long == rhs.long &&
    lhs.e_w == rhs.e_w &&
    lhs.quality == rhs.quality &&
    lhs.sats == rhs.sats &&
    lhs.hdop == rhs.hdop &&
    lhs.alt == rhs.alt &&
    lhs.units == rhs.units &&
    lhs.geo_sep == rhs.geo_sep;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::gps_driver::raw_data_<ContainerAllocator1> & lhs, const ::gps_driver::raw_data_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace gps_driver

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsFixedSize< ::gps_driver::raw_data_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::gps_driver::raw_data_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::gps_driver::raw_data_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::gps_driver::raw_data_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::gps_driver::raw_data_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::gps_driver::raw_data_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::gps_driver::raw_data_<ContainerAllocator> >
{
  static const char* value()
  {
    return "a8fc4d15e90cc4fb2818dc101c57a5bb";
  }

  static const char* value(const ::gps_driver::raw_data_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xa8fc4d15e90cc4fbULL;
  static const uint64_t static_value2 = 0x2818dc101c57a5bbULL;
};

template<class ContainerAllocator>
struct DataType< ::gps_driver::raw_data_<ContainerAllocator> >
{
  static const char* value()
  {
    return "gps_driver/raw_data";
  }

  static const char* value(const ::gps_driver::raw_data_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::gps_driver::raw_data_<ContainerAllocator> >
{
  static const char* value()
  {
    return "string type\n"
"string utc_position \n"
"string lat\n"
"string n_s\n"
"string long\n"
"string e_w\n"
"string quality\n"
"string sats\n"
"string hdop\n"
"string alt\n"
"string units\n"
"string geo_sep\n"
;
  }

  static const char* value(const ::gps_driver::raw_data_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::gps_driver::raw_data_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.type);
      stream.next(m.utc_position);
      stream.next(m.lat);
      stream.next(m.n_s);
      stream.next(m.long);
      stream.next(m.e_w);
      stream.next(m.quality);
      stream.next(m.sats);
      stream.next(m.hdop);
      stream.next(m.alt);
      stream.next(m.units);
      stream.next(m.geo_sep);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct raw_data_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::gps_driver::raw_data_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::gps_driver::raw_data_<ContainerAllocator>& v)
  {
    s << indent << "type: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.type);
    s << indent << "utc_position: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.utc_position);
    s << indent << "lat: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.lat);
    s << indent << "n_s: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.n_s);
    s << indent << "long: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.long);
    s << indent << "e_w: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.e_w);
    s << indent << "quality: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.quality);
    s << indent << "sats: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.sats);
    s << indent << "hdop: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.hdop);
    s << indent << "alt: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.alt);
    s << indent << "units: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.units);
    s << indent << "geo_sep: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.geo_sep);
  }
};

} // namespace message_operations
} // namespace ros

#endif // GPS_DRIVER_MESSAGE_RAW_DATA_H
