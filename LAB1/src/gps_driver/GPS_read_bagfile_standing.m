clear
clc

%Extracting topic from bagfile
filename = 'gps_record_standing1.bag';
bag = rosbag(filename);
bagInfo = rosbag('info',filename);
rosbag info 'gps_record_standing1.bag'
bSel = select(bag,'Topic','/parsed_data_topic');
msgStructs = readMessages(bSel,'DataFormat','struct');
%msgStructs{1}

%Extracting message data from topic
Latitude = cellfun(@(m) string(m.Latitude),msgStructs);
Longitude = cellfun(@(m) string(m.Longitude),msgStructs);
Altitude = cellfun(@(m) string(m.Altitude),msgStructs);
UTM_Easting = transpose(str2double(cellfun(@(m) string(m.UTMEasting),msgStructs)));
UTM_Northing = transpose(str2double(cellfun(@(m) string(m.UTMNorthing),msgStructs)));
Zone = cellfun(@(m) string(m.Zone),msgStructs);
Letter = cellfun(@(m) string(m.Letter),msgStructs);

Time = cellfun(@(m) (m.Header.Stamp.Sec),msgStructs);
T0 = Time(1);
Time = transpose(Time - T0);
%-----------------------------------------------------------

%Graphing GPS Output Relative to Known Position 
known_xy = [331510.14,4687609.37];
figure('name','GPS Output Relative to Known Position');
plot(UTM_Easting-known_xy(1),UTM_Northing-known_xy(2));
xlabel('UTM Easting (m)');
ylabel('UTM Northing (m)');
title('GPS Output Relative to Known Position')
hold on
plot(0,0,'xr');

%Graphing Error in Position over Time
known_xy = [331510.14,4687609.37];
error_xy = ((UTM_Easting-known_xy(1)).^2+(UTM_Northing-known_xy(2)).^2).^(1/2);
figure('name','Error in Position vs Time');
plot(Time, error_xy);
%axis([0, 2, 0, .5]);
xlabel('Time (s)');
ylabel('Error (m)');
title('Position Error vs Time')

%Graphing Histogram of Error Data
figure('name','Histogram');
histogram(error_xy)
%axis([1, 2.5, 0, 200]);
xlim([1, 2.5]);
xlabel('Error (m)');
ylabel('Frequency');
title('Position Error Density')

mean(error_xy)
std(error_xy)

%{
%Graphing Frequency of Error Data
n=length(error_xy);
values = zeros(2,26);
pointer = 1;
save = true;
for i =1:1:n
  for j=1:1:pointer
    if(error_xy(i) == values(1,j))
        save = false;
        values(2,j) = values(2,j)+1;
        break;
    end
  end
  if(save == true)
     values(1,pointer) = error_xy(i);
     values(2,pointer) = 1;
     pointer = pointer+1;
  end
   save = true; 
end
values = transpose(sortrows(transpose(values)))
%values(2,:) = values(2,:)./n

figure('name','UTM Easting');
plot(values(1,:),values(2,:));
%plot(values);
%bar(values)
axis([1, 2.5, 0, 200]);
xlabel('Error (m)');
ylabel('Density(% of data points) (%)');
title('Error Density')
%}

%{
fileNew = 'myfile.txt'
fid = fopen(fileNew,'w')
fprintf(fid,'Time       UTM Easting        UTM Northing\n')
for i = 1:1:length(UTM_Easting)
    fprintf(fid,'%1.2f          %1.6f              %1.6f\n',Time(i),UTM_Easting(i),UTM_Northing(i));
    
    end
    fclose(fid)
    %}