clear
clc

%Extracting topic from bagfile
filename = 'gps_record_line1.bag';
bag = rosbag(filename);  
bagInfo = rosbag('info',filename);
rosbag info 'gps_record_line1.bag'
bSel = select(bag,'Topic','/parsed_data_topic'); 
msgStructs = readMessages(bSel,'DataFormat','struct'); 

%Extracting message data from topic
Latitude = cellfun(@(m) string(m.Latitude),msgStructs);
Longitude = cellfun(@(m) string(m.Longitude),msgStructs);
Altitude = cellfun(@(m) string(m.Altitude),msgStructs);
UTM_Easting = transpose(str2double(cellfun(@(m) string(m.UTMEasting),msgStructs)));
UTM_Northing = transpose(str2double(cellfun(@(m) string(m.UTMNorthing),msgStructs)));
Zone = cellfun(@(m) string(m.Zone),msgStructs);
Letter = cellfun(@(m) string(m.Letter),msgStructs);

Time = cellfun(@(m) (m.Header.Stamp.Sec),msgStructs);
T0 = Time(1);
Time = transpose(Time - T0);
%-----------------------------------------------


%Graphing GPS Output Relative to Known Path
figure('name','line');
%Trim stationary
UTM_Easting = UTM_Easting(3:length(UTM_Easting)-4);
UTM_Northing = UTM_Northing(3:length(UTM_Northing)-4);
start_xy = [331511.205776,4687613.6026];
end_xy = [331556.358205, 4687508.26289];
plot(start_xy(1),start_xy(2),'sb');
hold on
plot(end_xy(1),end_xy(2),'sb');
plot(UTM_Easting,UTM_Northing,'.r');
x = linspace(start_xy(1),end_xy(1),126);
y = linspace(start_xy(2),end_xy(2),126);
plot(x,y,'-b');
xlabel('UTM Easting (m)');
ylabel('UTM Northing (m)');
title('GPS Output Relative to Known Path');
hold on

%Graphing Histogram of Error Data
figure('name','Histogram');
error = sqrt((y-UTM_Northing).^2+(x-UTM_Easting).^2);
histogram(error,20);
%axis([1, 2.5, 0, 200]);
xlim([0, 1.6]);
ylim([0,25]);
xlabel('Error (m)');
ylabel('Frequency');
title('Position Error Density');


%{
fileNew = 'myfileline.txt'
fid = fopen(fileNew,'w')
fprintf(fid,'Time       UTM Easting        UTM Northing\n')
for i = 1:1:length(UTM_Easting)
    fprintf(fid,'%1.2f          %1.6f              %1.6f\n',Time(i),UTM_Easting(i),UTM_Northing(i));
    
end
fclose(fid)
%}
