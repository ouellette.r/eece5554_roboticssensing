clear
clc
close all
format long

%This is the data collected during 10 minute stationary test
%Extracting topic from stationary bagfile
filename = '5.bag';
bag = rosbag(filename);  
bagInfo = rosbag('info',filename);
rosbag info '5.bag'
bSel = select(bag,'Topic','/imu/IMU_parsed');
msgStructs = readMessages(bSel,'DataFormat','struct'); 

%Extracting message data from topic
TimeS = cast(cellfun(@(m) (m.Header.Stamp.Sec),msgStructs),'double');
TimeNS = cast(cellfun(@(m) (m.Header.Stamp.Nsec),msgStructs),'double');
T0 = TimeS(1,1);
TimeS = TimeS - T0;
Time = TimeS + 1e-9.*TimeNS;
%{
%Extracting quaternion data
quat = [cellfun(@(m) double(m.Orientation.X),msgStructs),...
    cellfun(@(m) double(m.Orientation.Y),msgStructs),...
    cellfun(@(m) double(m.Orientation.Z),msgStructs),...
    cellfun(@(m) double(m.Orientation.W),msgStructs)];
%converting quaternions into euler angles in radian
eulZYX = quat2eul(quat);
%Converting euler angles from radians to degrees
roll = rad2deg(eulZYX(:,1));
pitch = rad2deg(eulZYX(:,2));
yaw = rad2deg(eulZYX(:,3));
%}

%Extracting angular velocity data
vx = cellfun(@(m) double(m.AngularVelocity.X),msgStructs);
vy = cellfun(@(m) double(m.AngularVelocity.Y),msgStructs);
vz = cellfun(@(m) double(m.AngularVelocity.Z),msgStructs);

%Extracting linear acceleration data (Gyro)
ax = cellfun(@(m) double(m.LinearAcceleration.X),msgStructs);
ay = cellfun(@(m) double(m.LinearAcceleration.Y),msgStructs);
az = cellfun(@(m) double(m.LinearAcceleration.Z),msgStructs);
%-----------------------------------------------


%Graphing Acceleration in x direction versus time
figure('name','Ax vs Time');
plot(Time,ax,'.r');
%axis([0,1000,-10, -9.7]);
xlabel('Time (s)');
ylabel('X Acceleration (m/s^2)');
title('X Acceleration vs. Time');
hold on
%print('-clipboard','-dmeta')


%Graphing Acceleration in x direction versus time
figure('name','Angular Velocity vs Time');
plot(Time,vx,'.r');
%axis([0,1000,-10, -9.7]);
xlabel('Time (s)');
ylabel('Angular Velocity X (rad/s)');
title('Angular Velocity X vs. Time');
hold on
print('-clipboard','-dmeta')




% Load logged data from one axis of a three-axis gyroscope. This recording
% was done over a five hour period with a 40 Hz sampling rate.
Fs = 40;
t0 = 1/Fs;

theta = cumsum(vx,1)*t0;
maxNumM = 100;
L = size(theta, 1);
maxM = 2.^floor(log2(L/2));
m = logspace(log10(1), log10(maxM), maxNumM).';
m = ceil(m); % m must be an integer.
m = unique(m); % Remove duplicates.

tau = m*t0;

avar = zeros(numel(m), 1);
for i = 1:numel(m)
    mi = m(i);
    avar(i,:) = sum( ...
        (theta(1+2*mi:L) - 2*theta(1+mi:L-mi) + theta(1:L-2*mi)).^2, 1);
end
avar = avar ./ (2*tau.^2 .* (L - 2*m));

adev = sqrt(avar);

figure
loglog(tau, adev)
title('Allan Deviation')
xlabel('\tau');
ylabel('\sigma(\tau)')
grid on
axis equal

% Find the index where the slope of the log-scaled Allan deviation is equal
% to the slope specified.
slope = -0.5;
logtau = log10(tau);
logadev = log10(adev);
dlogadev = diff(logadev) ./ diff(logtau);
[~, i] = min(abs(dlogadev - slope));

% Find the y-intercept of the line.
b = logadev(i) - slope*logtau(i);

% Determine the angle random walk coefficient from the line.
logN = slope*log(1) + b;
N = 10^logN

% Plot the results.
tauN = 1;
lineN = N ./ sqrt(tau);
figure
loglog(tau, adev, tau, lineN, '--', tauN, N, 'o')
title('Allan Deviation with Angle Random Walk')
xlabel('\tau')
ylabel('\sigma(\tau)')
legend('\sigma', '\sigma_N')
text(tauN, N, 'N')
grid on
axis equal

% Find the index where the slope of the log-scaled Allan deviation is equal
% to the slope specified.
slope = 0.5;
logtau = log10(tau);
logadev = log10(adev);
dlogadev = diff(logadev) ./ diff(logtau);
[~, i] = min(abs(dlogadev - slope));

% Find the y-intercept of the line.
b = logadev(i) - slope*logtau(i);

% Determine the rate random walk coefficient from the line.
logK = slope*log10(3) + b;
K = 10^logK

% Plot the results.
tauK = 3;
lineK = K .* sqrt(tau/3);
figure
loglog(tau, adev, tau, lineK, '--', tauK, K, 'o')
title('Allan Deviation with Rate Random Walk')
xlabel('\tau')
ylabel('\sigma(\tau)')
legend('\sigma', '\sigma_K')
text(tauK, K, 'K')
grid on
axis equal

% Find the index where the slope of the log-scaled Allan deviation is equal
% to the slope specified.
slope = 0;
logtau = log10(tau);
logadev = log10(adev);
dlogadev = diff(logadev) ./ diff(logtau);
[~, i] = min(abs(dlogadev - slope));

% Find the y-intercept of the line.
b = logadev(i) - slope*logtau(i);

% Determine the bias instability coefficient from the line.
scfB = sqrt(2*log(2)/pi);
logB = b - log10(scfB);
B = 10^logB

% Plot the results.
tauB = tau(i);
lineB = B * scfB * ones(size(tau));
figure
loglog(tau, adev, tau, lineB, '--', tauB, scfB*B, 'o')
title('Allan Deviation with Bias Instability')
xlabel('\tau')
ylabel('\sigma(\tau)')
legend('\sigma', '\sigma_B')
text(tauB, scfB*B, '0.664B')
grid on
axis equal

tauParams = [tauN, tauK, tauB];
params = [N, K, scfB*B];
figure
loglog(tau, adev, tau, [lineN, lineK, lineB], '--', ...
    tauParams, params, 'o')
title('Allan Deviation with Noise Parameters')
xlabel('\tau')
ylabel('\sigma(\tau)')
legend('\sigma', '\sigma_N', '\sigma_K', '\sigma_B')
text(tauParams, params, {'N', 'K', '0.664B'})
grid on
axis equal
print('-clipboard','-dmeta')
