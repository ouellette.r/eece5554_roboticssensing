clear
clc
format long
%Extracting topic from bagfile
filename = 'imu_stationary.bag';
bag = rosbag(filename);  
bagInfo = rosbag('info',filename);
rosbag info 'imu_stationary.bag'
bSel = select(bag,'Topic','/imu_parsed_topic');
msgStructs = readMessages(bSel,'DataFormat','struct'); 

%Extracting message data from topic

%Extracting quaternion data
quat = [cellfun(@(m) double(m.Orientation.X),msgStructs),...
    cellfun(@(m) double(m.Orientation.Y),msgStructs),...
    cellfun(@(m) double(m.Orientation.Z),msgStructs),...
    cellfun(@(m) double(m.Orientation.W),msgStructs)];
%converting quaternions into euler angles in radian
eulZYX = quat2eul(quat);
%Converting euler angles from radians to degrees
roll = rad2deg(eulZYX(:,1));
pitch = rad2deg(eulZYX(:,2));
yaw = rad2deg(eulZYX(:,3));

%Extracting angular velocity data
vx = cellfun(@(m) double(m.AngularVelocity.X),msgStructs);
vy = cellfun(@(m) double(m.AngularVelocity.Y),msgStructs);
vz = cellfun(@(m) double(m.AngularVelocity.Z),msgStructs);

%Extracting linear acceleration data
ax = cellfun(@(m) double(m.LinearAcceleration.X),msgStructs);
ay = cellfun(@(m) double(m.LinearAcceleration.Y),msgStructs);
az = cellfun(@(m) double(m.LinearAcceleration.Z),msgStructs);
%TimeS = double(TimeS)
%typecast(TimeS, 'double')
%class(TimeS)
%class(TimeNS)
%Time = TimeS +.02;
%Time = transpose(Time - T0);
%
TimeS = cellfun(@(m) (m.Header.Stamp.Sec),msgStructs);
TimeNS = cellfun(@(m) (m.Header.Stamp.Nsec),msgStructs);
T0 = TimeS(1,1);
TimeS = TimeS - T0;
Time = TimeS + 10e-9.*TimeNS;
%-----------------------------------------------


%{
%Graphing Acceleration in Z direction versus time
figure('name','AZ vs Time');
plot(Time,az,'.r');
axis([0,1000,-10, -9.7]);
xlabel('Time (s)');
ylabel('Z Acceleration (m/s^2)');
title('Z Acceleration vs. Time');
hold on

%Graphing Histogram of Acceleration in Z direction Error
figure('name','Histogram AZ Error');
Maz = mean(az);
azError = az-Maz;
histogram(azError);
axis([-.08, .08, 0, 2000]);
xlabel('Error (m/s^2)');
ylabel('Frequency');
title('Z Acceleration');
%}
%{
%Graphing Acceleration in X direction versus time
figure('name','AX vs Time');
plot(Time,ax,'.r');
axis([0,1000,-.15, .15]);
xlabel('Time (s)');
ylabel('X Acceleration (m/s^2)');
title('X Acceleration vs. Time');
hold on

%Graphing Histogram of Acceleration in X direction Error
figure('name','Histogram AX Error');
Max = mean(ax);
axError = ax-Max;
histogram(azError);
axis([-.08, .08, 0, 2000]);
xlabel('Error (m/s^2)');
ylabel('Frequency');
title('X Acceleration');

%Graphing Acceleration in Y direction versus time
figure('name','AY vs Time');
plot(Time,ay,'.r');
axis([0,1000,-.15, .15]);
xlabel('Time (s)');
ylabel('Y Acceleration (m/s^2)');
title('Y Acceleration vs. Time');
hold on

%Graphing Histogram of Acceleration in Y direction Error
figure('name','Histogram AY Error');
May = mean(ay);
ayError = ay-May;
histogram(ayError);
axis([-.08, .08, 0, 2200]);
xlabel('Error (m/s^2)');
ylabel('Frequency');
title('Y Acceleration');
%}
%{
%Graphing Linear Velocity in X direction versus time
figure('name','VX vs Time');
plot(Time,vx,'.r');
axis([0,1000,-.005, .005]);
xlabel('Time (s)');
ylabel('X Velocity (m/s)');
title('X Velocity vs. Time');
hold on

%Graphing Histogram of Acceleration in Z direction Error
figure('name','Histogram VX Error');
Mvx = mean(vx);
vxError = vx-Mvx;
histogram(vxError);
axis([-.003, .003, 0, 1400]);
xlabel('Error (m/s)');
ylabel('Frequency');
title('X Velocity');
%}


%Graphing Roll versus time
figure('name','Roll vs Time');
plot(Time,roll,'.r');
%axis([0,1000,-.005, .005]);
xlabel('Time (s)');
ylabel('Roll (Degrees)');
title('Roll vs. Time');
hold on

%Graphing Histogram of Acceleration in Z direction Error
figure('name','Histogram Roll Error');
Mroll = mean(roll);
rollError = roll-Mroll;
histogram(rollError);
%axis([-.003, .003, 0, 1400]);
xlabel('Error (degrees)');
ylabel('Frequency');
title('Roll');


