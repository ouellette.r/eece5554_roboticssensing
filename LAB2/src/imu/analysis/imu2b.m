clear
clc
close all
format long

%This is the data collected during 10 minute stationary test
%Extracting topic from stationary bagfile
filename = 'imu_stationary.bag';
bag = rosbag(filename);  
bagInfo = rosbag('info',filename);
rosbag info 'imu_stationary.bag'
bSel3 = select(bag,'Topic','/mag_parsed_topic');
msgStructs3 = readMessages(bSel3,'DataFormat','struct'); 

%Extracting message data from topic
TimeS3 = cast(cellfun(@(m) (m.Header.Stamp.Sec),msgStructs3),'double');
TimeNS3 = cast(cellfun(@(m) (m.Header.Stamp.Nsec),msgStructs3),'double');
T03 = TimeS3(1,1);
TimeS3 = TimeS3 - T03;
Time3 = TimeS3 + 1e-9.*TimeNS3;

%Extracting magnetometer data
sat_mx = cellfun(@(m) double(m.MagneticField.X),msgStructs3);
sat_my = cellfun(@(m) double(m.MagneticField.Y),msgStructs3);
sat_mz = cellfun(@(m) double(m.MagneticField.Z),msgStructs3);

error_mx = mean(sat_mx);
error_my = mean(sat_my);

%{
%Graphing Stationary magnetometer data to see bias 
figure('name','Stationary Magnetometer');
plot(Time3,sat_my);
%axis([0,1000,-10, -9.7]);
xlabel('Time (s)');
ylabel('mag (gauss)');
title('mag vs Time - raw');
%}


%--------------------------------------------------------------------












%This is the Magnetometer data collected during the roundabout drive
%Extracting parsed_magnetic_field_data topic from bagfile
filename = 'Roundabout.bag';
bag = rosbag(filename); 
bagInfo = rosbag('info',filename);
rosbag info 'Roundabout.bag';
bSel = select(bag,'Topic','imu_driver/parsed_magnetic_field_data');
msgStructs = readMessages(bSel,'DataFormat','struct'); 

%Extracting message data from topic
TimeS = cast(cellfun(@(m) (m.Header.Stamp.Sec),msgStructs),'double');
TimeNS = cast(cellfun(@(m) (m.Header.Stamp.Nsec),msgStructs),'double');
T0 = TimeS(1,1);
TimeS = TimeS - T0;
Time = TimeS + 1e-9.*TimeNS;

%Extracting magnetometer data
raw_mx = cellfun(@(m) double(m.MagneticField.X),msgStructs);
raw_my = cellfun(@(m) double(m.MagneticField.Y),msgStructs);
raw_mz = cellfun(@(m) double(m.MagneticField.Z),msgStructs);

%raw_mx = raw_mx - raw_mx(1,1);
%raw_my = raw_my - raw_my(1,1);


%Graphing raw magnetometer x and y
figure('name','Raw Magnetometer XY');
title('Raw Magnetometer XY');
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
axis([-.02,.1,-.5, .1]);
xlh = xlabel('Magnetic Field X (Gauss)');
xlh.Position(2) = xlh.Position(2) +.1;
xlh.Position(1) = xlh.Position(1) +.005;
set(xlh, 'Rotation',90)
ylh = ylabel('Magnetic Field Y (Gauss)');
ylh.Position(1) = ylh.Position(1);
ylh.Position(2) = ylh.Position(2);
hold on
plot(raw_mx,raw_my,'.r');
%print('-clipboard','-dmeta')

%{
%3D Plot of mx and my vs time
figure('name','mx,my,time');
plot3(raw_mx,raw_my,Time,'.r')
%}

%correcting magnetic field data
%Grabbing data just after test started
cut = max(find(Time<=32.5));
mx = raw_mx(cut:end,1);
my = raw_my(cut:end,1);

%hard-iron correction
beta = (max(mx)+min(mx))/2;
alpha = (max(my)+min(my))/2;
mx = mx-beta;
my = my-alpha;
%soft-iron correction
%Calculating Major Axis of Ellipse
r = sqrt(mx.^2+my.^2);
x1 = mx(find(r==max(r)),1);
y1 = my(find(r==max(r)),1);
%Finding rotation angle
theta = asind(y1/max(r));
%Applying rotation transformation
v1 = [cosd(theta), sind(theta);-sind(theta),cosd(theta)]*[transpose(mx);transpose(my)];
%Finding minor axis
index = find(abs(v1(1,:))<.001);
q = sqrt(v1(1,index(1))^2+v1(2,index(1))^2);
%Scaling ellipse into circle
scale = q/max(r);
v1(1,:) = v1(1,:).*scale;
%Inverse rotation transformation
v1 = [cosd(-theta), sind(-theta);-sind(-theta),cosd(-theta)]*v1;
mx = v1(1,:);
my = v1(2,:);


%Graphing corrected magnetometer data x and y
figure('name','Corrected Magnetometer XY');
title('Corrected Magnetometer XY');
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
axis([-.05,.05,-.05, .05]);
xlh = xlabel('Magnetic Field X (Gauss)');
xlh.Position(2) = xlh.Position(2) +.02;
xlh.Position(1) = xlh.Position(1) +.01;
set(xlh, 'Rotation',90)
ylh = ylabel('Magnetic Field Y (Gauss)');
ylh.Position(1) = ylh.Position(1) +.002;
ylh.Position(2) = ylh.Position(2) +.002;
hold on
plot(mx,my,'.r');
print('-clipboard','-dmeta')


%{
figure('name','Corrected Magnetometer XY ------test');
title('Corrected Magnetometer XY -- -test');
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
axis([-.05,.05,-.05, .05]);
xlh = xlabel('Magnetic Field X (Gauss)');
xlh.Position(2) = xlh.Position(2) +.02;
xlh.Position(1) = xlh.Position(1) +.01;
set(xlh, 'Rotation',90)
ylh = ylabel('Magnetic Field Y (Gauss)');
ylh.Position(1) = ylh.Position(1) +.002;
ylh.Position(2) = ylh.Position(2) +.002;
hold on
plot(v21(1,:),v21(2,:),'.r');
%}


%3D Plot of mx and my vs time
%figure('name','new mx,my,time');
%plot3(mx,my,Time(cut:end,1),'.r')


%{
%Graphing mx vs time 
figure('name','mx vs time');
plot(Time(cut:end,1),mx);
%axis([0,1000,-10, -9.7]);
xlabel('Time (s)');
ylabel('mx ');
title('mx vs Time ');
hold on
%}



%--------------------------------------------------------------------

%This is the Magnetometer data collected during the drive
%Extracting parsed magnetic field data topic from bagfile
filename = 'Drive.bag';
bag = rosbag(filename); 
bagInfo = rosbag('info',filename);
rosbag info 'Drive.bag';
bSel3 = select(bag,'Topic','imu_driver/parsed_magnetic_field_data');
msgStructs3 = readMessages(bSel3,'DataFormat','struct');

%Extracting message data from topic
TimeS = cast(cellfun(@(m) (m.Header.Stamp.Sec),msgStructs3),'double');
TimeNS = cast(cellfun(@(m) (m.Header.Stamp.Nsec),msgStructs3),'double');
T0 = TimeS(1,1);
TimeS = TimeS - T0;
Time = TimeS + 1e-9.*TimeNS;

%Extracting magnetometer data
raw_mx = cellfun(@(m) double(m.MagneticField.X),msgStructs3);
raw_my = cellfun(@(m) double(m.MagneticField.Y),msgStructs3);
raw_mz = cellfun(@(m) double(m.MagneticField.Z),msgStructs3);

%Correcting magnetometer data for hard and soft iron effects
mx = raw_mx-beta;
my = raw_my-alpha;
V1 = [cosd(theta), sind(theta);-sind(theta),cosd(theta)]*[transpose(mx);transpose(my)];
V1(1,:) = V1(1,:).*scale;
V1 = [cosd(-theta), sind(-theta);-sind(-theta),cosd(-theta)]*V1;
mx = V1(1,:);
my = V1(2,:);
mz = raw_mz;

%{
%Graphing  
figure('name','corrected mx vs time');
plot(Time,mx);
xlabel('Time (s)');
ylabel('mx ');
hold on
%}





%This is the IMU data collected during the drive
%Extracting parsed imu data topic from bagfile
filename = 'Drive.bag';
bag = rosbag(filename); 
bagInfo = rosbag('info',filename);
rosbag info 'Drive.bag';
bSel2 = select(bag,'Topic','imu_driver/parsed_imu_data');
msgStructs2 = readMessages(bSel2,'DataFormat','struct');


%Extracting quaternion data
quat = [cellfun(@(m) double(m.Orientation.X),msgStructs2),...
    cellfun(@(m) double(m.Orientation.Y),msgStructs2),...
    cellfun(@(m) double(m.Orientation.Z),msgStructs2),...
    cellfun(@(m) double(m.Orientation.W),msgStructs2)];

%converting quaternions into euler angles in radian
eulZYX = quat2eul(quat);
%Converting euler angles from radians to degrees
roll = rad2deg(eulZYX(:,1));
pitch = rad2deg(eulZYX(:,2));
%yaw = rad2deg(eulZYX(:,3));
%yaw = (eulZYX(:,3));
yaw = unwrap(eulZYX(:,3));
yaw = rad2deg(yaw-yaw(cut,1));      %subtract initial yaw angle at start of record 



%calculating yaw from magnetometer data
yaw_mag = atan2d(transpose(-my).*cosd(roll)+ mz.*sind(roll),transpose(mx).*cosd(pitch)+transpose(my).*sind(pitch).*sind(roll)+mz.*sind(pitch).*cosd(roll));
yaw_mag = -1*rad2deg(unwrap(deg2rad(yaw_mag-yaw_mag(1,1))));
%yaw_mag = (atan2(-my,mx));
%yaw_mag = rad2deg(unwrap(deg2rad(yaw_mag-yaw_mag(1,1))));
%yaw_mag = atan2((-my.*cosd(roll) + mz.*sind(roll)),(mx.*cosd(pitch) + my.*sind(pitch).*sind(roll)+ mz.*sind(pitch).*cosd(roll)))
%transpose(rad2deg(unwrap(yaw_mag-yaw_mag(1,1))));



%Extracting angular velocity data
wx = cellfun(@(m) double(m.AngularVelocity.X),msgStructs2);
wy = cellfun(@(m) double(m.AngularVelocity.Y),msgStructs2);
wz = cellfun(@(m) double(m.AngularVelocity.Z),msgStructs2); %want this one - its in rad/s

%Calculating yaw by integrating angular velocity around z axis over time
yaw_gyro = rad2deg(cumtrapz(Time,wz));

%Complementary Filter (deg)
comp_yaw = yaw_gyro*.9 + yaw_mag*.1;

%Graphing yaw vs time  
figure('name','Yaw vs Time');
plot(Time,yaw_mag);
xlabel('Time (s)');
ylabel('Yaw (degrees)');
hold on
plot(Time,yaw);
plot(Time,yaw_gyro);
plot(Time,comp_yaw);
title('Yaw vs Time');
legend('Magnetometer','IMU','Gyro','Complementary')
print('-clipboard','-dmeta')




%---------------------------------------------------------------



%Extracting linear acceleration data (m/s^2)
ax = cellfun(@(m) double(m.LinearAcceleration.X),msgStructs2);
ay = cellfun(@(m) double(m.LinearAcceleration.Y),msgStructs2);
az = cellfun(@(m) double(m.LinearAcceleration.Z),msgStructs2);

%Adjusting acceleration so mean is closer to zero
ax = ax - mean(ax);


figure('name','Ax vs Time');
plot(Time,ax);
xlabel('Time (s)');
ylabel('Acceleration (m/s^2)');
hold on
print('-clipboard','-dmeta')





%times where car is stationary
spots = [1,46,48,87,91,133,155,182,187,190,198,210,231,264,295,381,398,455,472,474,557,593,621,623,639,641,691];

%Adjust acceleration over time interval between stops so that mean is close
%to zero - removes bias over interval
for i =1:1:length(spots)-1
    
    spotA = min(find(Time>spots(i)));
    spotB = min(find(Time>spots(i+1)));
    mean(ax(spotA:spotB,1));
    
    ax(spotA:spotB,1) = ax(spotA:spotB,1)-mean(ax(spotA:spotB,1));
    mean(ax(spotA:spotB,1));
    
end

%Calculate velocity by intergrating over acceleration
v_imu = (cumtrapz(Time,ax));
    
    
%This is the GPS data collected during the drive
%Extracting parsed_gps_data topic from bagfile
filename = 'Drive.bag';
bag = rosbag(filename); 
bagInfo = rosbag('info',filename);
rosbag info 'Drive.bag';
bSel4 = select(bag,'Topic','gps_sensor/parsed_gps_data');
msgStructs4 = readMessages(bSel4,'DataFormat','struct');

%Extracting message data from topic
TimeS_GPS = cast(cellfun(@(m) (m.Header.Stamp.Sec),msgStructs4),'double');
TimeNS_GPS = cast(cellfun(@(m) (m.Header.Stamp.Nsec),msgStructs4),'double');
T0_GPS = TimeS_GPS(1,1);
TimeS_GPS = TimeS_GPS - T0_GPS;
TimeG = TimeS_GPS + 1e-9.*TimeNS_GPS;

%Extracting UTM data (m)
UTM_E = cellfun(@(m) double(m.UtmEasting),msgStructs4);
UTM_N = cellfun(@(m) double(m.UtmNorthing),msgStructs4);
%{
%Graphing UTM  
figure('name','UTM');
plot3(UTM_E,UTM_N,TimeG);
xlabel('UTM E');
ylabel('UTM N');
zlabel('time');
hold on
%}
%Calculating velocity by integrating UTM over time
pos = sqrt(UTM_E.^2+UTM_N.^2);
v_gps = abs(diff(pos)./diff(TimeG));
%{
%Graphing UTM  
figure('name','position');
plot(TimeG,pos);
xlabel('time');
ylabel('pos');
hold on
%}



%Graphing velocity vs time  
figure('name','Velocity vs Time');
plot(Time,v_imu);
xlabel('Time (s)');
ylabel('Velocity (m/s)');
hold on
plot(TimeG(1:end-1),v_gps);
title('Velocity vs Time');
legend('IMU','GPS')
%print('-clipboard','-dmeta')



%------------------------------------------
%Dead Recknoning

%compute wz*Xdot using x velocity from integrating x acceleration
%integrating x acceleration to get x velocity
%%%%%%vx = cumtrapz(Time,ax);
vx = v_imu;
%x velocity times angular velocity in z direction gives y acceleration
wzx = vx.*wz; 
%wzx = v_imu.*wz;
%Graphing acceleration versus time  
figure('name','Acceleration vs Time');
plot(Time,ay);
hold on
xlabel('Time (s)');
ylabel('Acceleration (m/s^2)');
plot(Time,wzx);
title('Acceleration vs Time');
legend('From X Velocity','Y Acceleration')
print('-clipboard','-dmeta')

%rotate heading into fixed east north
ve = vx.*sin(deg2rad(comp_yaw-40));
vn = vx.*cos(deg2rad(comp_yaw-40));




xe = cumtrapz(Time,ve);
xe = xe - xe(1,1);
xn = cumtrapz(Time,vn);
xn = xn - xn(1,1);

UTM_E = UTM_E - UTM_E(1,1);
UTM_N = UTM_N - UTM_N(1,1);

%Plot UTM GPS trajectory and IMU trajectory
figure('name','UTM Trajectory');
axis square
plot(UTM_E,UTM_N,'r')
xlabel('UTM Easting (m)');
ylabel('UTM Northing (m)');
hold on
plot(xe,xn,'b');
title('UTM Position vs. Time');
legend('GPS','IMU')
print('-clipboard','-dmeta')

%{
figure('name','ve vn vs Time');
plot(Time,ve);
xlabel('Time (s)');
ylabel('v (m/s)');
hold on
plot(Time,vn);
%plot(Time,);
%plot(Time(ind(),1),12,'sb');
%title('Yaw vs Time');
legend('ve','vn')
%print('-clipboard','-dmeta')
%}




