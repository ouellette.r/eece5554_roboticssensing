#!/usr/bin/env python

## Reads VNYMR data from IMU vn-100 connected to serial port
import rospy
import serial
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField
import tf
from math import radians
from math import degrees

if __name__ == '__main__':
	
	rospy.init_node('imu_driver', anonymous=True)
	
	#Asking user for port address
	port_name = input("Enter port name as a string: Example: \"/dev/ttyUSB0\"\n")
	port_address = port_name

	#Establishing serial connection with device at port
	serial_port = rospy.get_param('~port',port_address)
	serial_baud = rospy.get_param('~baudrate',115200)
	port = serial.Serial(serial_port, serial_baud, timeout=3.)
	
	#Publisher for imu message
	pub_imu_parsed = rospy.Publisher('imu_parsed_topic', Imu, queue_size=10)
	parsed_imu_msg = Imu()

	#Publisher for magnetometer message
	pub_mag_parsed = rospy.Publisher('mag_parsed_topic', MagneticField, queue_size=10)
	parsed_mag_msg = MagneticField()

	try:
		while not rospy.is_shutdown():
			line = port.readline()
			#rospy.loginfo(line)
			time = rospy.Time.now()
			if line == '':
				rospy.loginfo("NO DATA")

			elif line.find("$VNYMR")!=-1:
				try:
					#Timestamping messsages
					parsed_imu_msg.header.stamp = time
					parsed_mag_msg.header.stamp = time
					
					#splitting line up into segments
					parts = line.split(",")
					#for part in parts:
						#rospy.loginfo(part)

					#separating yaw, pitch, roll and connverting from degrees to radians
					yaw = radians(float(parts[1]))
					pitch = radians(float(parts[2]))
					roll = radians(float(parts[3]))
					
					#converting yaw, pitch, roll from euler to quaternion
					quaternion = tf.transformations.quaternion_from_euler(roll, pitch, yaw)
			
					#writing orientation data to message
					parsed_imu_msg.orientation.x = quaternion[0]	
					parsed_imu_msg.orientation.y = quaternion[1]
					parsed_imu_msg.orientation.z = quaternion[2]
					parsed_imu_msg.orientation.w = quaternion[3]

					#writing acceleration data to message
					parsed_imu_msg.linear_acceleration.x = float(parts[7])
					parsed_imu_msg.linear_acceleration.y = float(parts[8])
					parsed_imu_msg.linear_acceleration.z = float(parts[9])

					#writing angular velocity data to message
					parsed_imu_msg.angular_velocity.x = float(parts[10])
					parsed_imu_msg.angular_velocity.y = float(parts[11])
					parsed_imu_msg.angular_velocity.z = float(parts[12][:parts[12].index('*')])

					#publishing imu message
					pub_imu_parsed.publish(parsed_imu_msg)

					#writing magnetometer data to message
					parsed_mag_msg.magnetic_field.x = float(parts[4])
					parsed_mag_msg.magnetic_field.y = float(parts[5])
					parsed_mag_msg.magnetic_field.z = float(parts[6])

					#publishing magnetometer message
					pub_mag_parsed.publish(parsed_mag_msg)

				except:
					rospy.loginfo("Unable to read data")
				
			else:
				rospy.loginfo(line)

	except rospy.ROSInterruptException:
		port.close()

	except serial.serialutil.SerialException:
		rospy.loginfo("Shutting down node...")