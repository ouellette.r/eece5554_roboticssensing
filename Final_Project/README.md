# EECE 5554 Spring 2021 Final Project - Visual SLAM on NUANCE IR Camera Using ORB-SLAM3​

Usage:

From the ORBSLAM3 folder after running ./build.sh, run:

./Examples/Monocular/mono_nuance Vocabulary/ORBvoc.txt Examples/Monocular/NUANCE_IR.yaml /path/to/images/folder/


Structure of images folder:
->IR_times.txt
->images/
--> 0.jpg
--> 1.jpg
.....

IR_times.txt is a file that has the timestamp in nanoseconds in order

