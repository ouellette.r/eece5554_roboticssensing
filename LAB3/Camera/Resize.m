clear
clc
close all

rawDir = fullfile('Brick_Wall/Crop');
resizedDir = 'Brick_Wall/Crop_Resized/';
rootResized = 'Brick_'
allImages = imageDatastore(rawDir);

% Display images to be stitched.
figure('name','All')
montage(allImages.Files)
numImages = numel(allImages.Files);

%window to show original images
figure('name','original');

for i=1:1:numImages
    % Read the first image from the image set.
    I = readimage(allImages,i);

    %show raw image
    %I = imrotate(I,-90);
    size(I)
    imshow(I)
    
        
    %Resize and show image
    %figure('name','resized')
    resizedimage = imresize(I,1/4);
    size(resizedimage)
    %imshow(resizedimage)

    
    
    
    %save image to resized folder
    imageName = [resizedDir,rootResized,num2str(i),'.jpg']
    imwrite(resizedimage, imageName);
end
%close(2)